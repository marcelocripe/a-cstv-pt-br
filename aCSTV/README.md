# aCSTV — antiX Community Simple TV

aCSTV is a tiny script turning your PC into a true TV, receiving local (and some foreign) stations via internet stream. It is multilingual and freely configurable, allowing to adapt it to the circumstances in your country.

## Installation

A.) Apt package installation (This is the preferred way of installation.)
- [ ] Download deb-installer-package and checksum file from respective folder. It is for antiX 17, 19, 21 and 22, and for 32bit and 64bit both.
- [ ] Check shasum file using the command:
```
shasum -c './aCSTV.deb.sha256.sum'
```
It must show an _OK_.
- [ ] If check was OK, install the package by the commands 
```
sudo apt-get update && sudo apt-get install './aCSTV.deb'
```
Check console output for error messages during installation.


B.) Manual installation (not recommended):
- [ ] Put the script file _aCSTV.sh_ from _bin_ directory into the antiX system folder _/usr/local/bin_ and make it executable (_sudo chmod 755 /usr/local/bin/aCSTV.sh_)
- [ ] Put the localisation of its user interface for your language (_.mo_ type file) into the respective locale subflolder of your antiX system. ( _/usr/share/locale/‹your_language_ID›/LC_MESSAGES_ ).
- [ ] Put the usermanual of your language (_.pdf_ file type) from _resources/helpfiles_ into the folder "/usr/local/lib/aCSTV/Handbücher". If the folder doesn't exist, create it.
- [ ] You'll need to register aCSTV.png and aCSTV2.png icons as named GTK icons in your system, otherwise it won't show properly in menu and Taskbar.
- [ ] In case there exists a stations list specific for your country already, put it also to the folder "/usr/local/lib/aCSTV/Stationslisten". If there is none by now, script will create a default list file containg German TV stations as an exmple. Modify its contents by deleting/editing/adding its entries using the _Settings_ and _Edit stations list_ buttons in running aCSTV program according to your needs. Your edited list will get saved as your country specific stations list after aCSTV is left.
- [ ] Make sure you have _mpv_, _yt-dlp_, _socat_, _buffer_, _gtkdialog_, _yad_, _xdotool_, _wmctrl_, _sed_ and _feh_ installed on your system and you can use _/dev/shm_ as _temp_ device. (this is all fine already on antiX 17, 19, 21 and 22)
- [ ] You may want to add an entry to your antiX program menu to start it other than entering its name in a shell console window.

## Usage
Either click on the menu entry in antiX program menu, or enter _aCSTV.sh_ in a command line window, e.g. in RoxTerm. The program help describing the general handling is available under _Settings → Program Help_ within its user interface.

## Support
For help, questions, suggestions and bug reporting please write to [antiXlinux forums](https://www.antixforum.com).

## Contributing
- If you find a translation of user interface or user manual being inadequat or misleading wrong, please edit it simply on [antix-contribs at transifex](https://www.transifex.com/antix-linux-community-contributions/antix-contribs/acstv-antix-community-simple-tv-starter/). On transifex you may edit the string translations used in user interface directly.For the help file there is an editable version of original help file and its English language translation present in _resources/helpfiles/editable_ subfolder (.odt file format). For the moment please upload your improved version of the translation of helpfile in your language as an attachment to a posting in antiX forums. Your improvements will get included into the .pdf resource files.
- If you can provide a recent stations list file working for your country, please let us know in antiX forums (see above) by attatching it to your posting there, so we can add it to the program resources, making it available for other people in your country this way.

## Authors and acknowledgment
This is an antiX community project.

## License
GPL Version 3 (GPLv3)

-----------
Written 2021/2022 by Robin.antiX for antiX community.


