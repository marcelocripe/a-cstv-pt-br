# aCSTV antiX Simple TV starter script ver. 1.13
# Copyright (C) 2021, 2022, 2023, 2024 The antiX community
# This file is distributed under the same license GPL V.3 as the aCSTV package.
# Robin 2024
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: aCSTV 1.13\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-01-20 17:06+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: antiX-contribs (transifex.com)\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

# Base filename for aCSTV settings file.
# Please consider the common conventions for file naming on antiX file
# systems.
#: aCSTV.sh:45
msgid "aCSTV-Einstellungen"
msgstr ""

# Base filename for aCSTV stations list file.
# Please consider the common conventions for file naming on antiX file
# systems.
#: aCSTV.sh:46
msgid "aCSTV-Sender"
msgstr ""

# Base filename for aCSTV usage instructions file
#: aCSTV.sh:47
msgid "Bedienungsanleitung"
msgstr ""

# header of command line help message. max. length 80 characters for console.
#: aCSTV.sh:74
msgid "aCSTV — antiX Community Simple TV                             Ver."
msgstr ""

# console help message text block, part 1.
# The text and the sentences can get freely spread over the following lines,
# but keep in mind you have 80 characters per line max.
#: aCSTV.sh:74
msgid "Dieses Script bietet eine intuitive und einfach zu bedienende Benutzer-"
msgstr ""

# console help message text block, part 2.
# The text and the sentences can get freely spread over the following lines,
# but keep in mind you have 80 characters per line max.
#: aCSTV.sh:75
msgid "oberfläche  zur Wiedergabe  regionaler oder  international  verfügbarer"
msgstr ""

# console help message text block, part 3.
# The text and the sentences can get freely spread over the following lines,
# but keep in mind you have 80 characters per line max.
#: aCSTV.sh:75
msgid "Fernsehsender als Internet Live-Stream. Die Wiedergabe von Live-Streams"
msgstr ""

# console help message text block, part 4.
# The text and the sentences can get freely spread over the following lines,
# but keep in mind you have 80 characters per line max.
#: aCSTV.sh:76
msgid "setzt eine bestehende  Internetverbindung voraus.  Eine Mitschnitt- und"
msgstr ""

# console help message text block, part 5.
# The text and the sentences can get freely spread over the following lines,
# but keep in mind you have 80 characters per line max.
#: aCSTV.sh:76
msgid "eine  Szenenfotofunktion sowie Schnellzugriff auf eine Programmvorschau"
msgstr ""

# console help message text block, part 6.
# The text and the sentences can get freely spread over the following lines,
# but keep in mind you have 80 characters per line max.
#: aCSTV.sh:77
msgid ""
"im Webbrowser ergänzen den Funktionsumfang. Die Wiedergabe erfolgt wahl-"
msgstr ""

# console help message text block, part 7.
# The text and the sentences can get freely spread over the following lines,
# but keep in mind you have 80 characters per line max.
#: aCSTV.sh:77
msgid "weise über den  primären PC-Bildschirm oder ein anderes an den Computer"
msgstr ""

# console help message text block, part 8.
# The text and the sentences can get freely spread over the following lines,
# but keep in mind you have 80 characters per line max.
#: aCSTV.sh:78
msgid "angeschlossenes  Anzeigegerät  wie z.B. ein  Fernseher.  Alle Parameter"
msgstr ""

# console help message text block, part 9.
# The text and the sentences can get freely spread over the following lines,
# but keep in mind you have 80 characters per line max.
#: aCSTV.sh:78
msgid "sind  frei konfigurierbar.  Weitere Informationen  in der Programmhilfe"
msgstr ""

# console help message text block, part 10.
# The text and the sentences can get freely spread over the following lines,
# but keep in mind you have 80 characters per line max.
# if you need even more lines in translation, add in this last line a »\n«
# type line break followed by two blanks, then start with your text for an
# 11. line and so on, as many lines you need.
#: aCSTV.sh:79
msgid "bei den »Einstellungen«."
msgstr ""

# sub header for console help
#: aCSTV.sh:79
msgid "Befehlszeilenoptionen:"
msgstr ""

# option description of console help. Add or remove as many dots from the
# spacing as you need to meet the 80 character restriction.
# Please make sure you translate the option »--Hilfe« to exactly
# the same expression used for the command line option above.
# Don't translate »-h«
#: aCSTV.sh:79
msgid "--Hilfe oder -h . . . . . . . . . . . . . . . Diese Kurzhilfe anzeigen."
msgstr ""

# option description of console help. Add or remove as many dots from the
# spacing as you need to meet the 80 character restriction.
# Please make sure you translate the option »--Senderliste« to exactly
# the same expression used for the command line option below.
# Don't translate »-s«
#: aCSTV.sh:80
msgid ""
"--Senderliste oder -s <Datei> . . . . . Datei als Senderliste verwenden."
msgstr ""

# option description of console help. Add or remove as many dots from the
# spacing as you need to meet the 80 character restriction.
# Please make sure you translate the option »--Rahmen« to exactly
# the same expression used for the command line option below.
# Don't translate »-r«
#: aCSTV.sh:81
msgid "--Rahmen oder -r  . . . . . . . . . . . Fensterrahmen anzeigen."
msgstr ""

# console help text block, (followed by the web address of antiX forum).
#: aCSTV.sh:82
msgid "Fragen, Anregungen und Fehlermeldungen bitte an:"
msgstr ""

# translatable command line option, preceeded by two dashes. Please make sure
# to match the comand name used in help text.
#: aCSTV.sh:91 aCSTV.sh:108 aCSTV.sh:115
msgid "Hilfe"
msgstr ""

# translatable command line option, preceeded by two dashes. Please make sure
# to match the comand name used in help text.
#: aCSTV.sh:91 aCSTV.sh:100
msgid "Rahmen"
msgstr ""

# translatable command line option, preceeded by two dashes. Please make sure
# to match the comand name used in help text.
#: aCSTV.sh:91 aCSTV.sh:110 aCSTV.sh:125
msgid "Senderliste"
msgstr ""

# Button engraving
#: aCSTV.sh:93 aCSTV.sh:550 aCSTV.sh:1187
msgid "Abbruch"
msgstr ""

# Error message on command line, followed by decried syntax.
#: aCSTV.sh:113
msgid "Ungültige Befehlszeilenoption:"
msgstr ""

# Error message on command line, first part of the sentence, preceded by
#  decried syntax, followed by scriptfile name
#: aCSTV.sh:114
msgid "Bitte rufen Sie"
msgstr ""

# Error message on command line, second part of the sentence, preceded by
# script file name, followed by translatable command line option "Hilfe"
#: aCSTV.sh:115
msgid "mit der Option"
msgstr ""

# Error message on command line, third part of the sentence, preceded by
# translatable command line option "Hilfe", followed by short option -h
#: aCSTV.sh:115 aCSTV.sh:125
msgid "oder"
msgstr ""

# Error message on command line, fourth (last) part of the sentence,
# preceded by short option -h. The full sentence reads:
# »Bitte rufen Sie acstv.sh mit der Option --Hilfe oder -h auf.«
#: aCSTV.sh:115
msgid "auf."
msgstr ""

# Error message on command line, followed by the name of decried stations list
# file.
#: aCSTV.sh:123
msgid "Datei nicht vorhanden:"
msgstr ""

# Error message on command line, first part of the sentence, preceded by
# the name of decried stations list file, followed by translatable command
# line option "Senderliste", the tanslated word "oder" and the short option -s
#: aCSTV.sh:125
msgid "Bitte geben Sie mit"
msgstr ""

# Error message on command line, second part of the sentence preceded by short
# option -s, followed by third part of the sentence
#: aCSTV.sh:125
msgid "eine gültige"
msgstr ""

# Error message on command line, third part of the sentence preceded by second
# part of the sentence. Full sentence reads: »Bitte geben Sie mit
# --Senderliste
# oder -s eine gültige Stationslistendatei an.«
#: aCSTV.sh:126
msgid "Stationslistendatei an."
msgstr ""

# translatable window title. Please make sure your translation is accepted by
# »xdotool« and »gtkdialog« as a valid window title.
#: aCSTV.sh:187 aCSTV.sh:507 aCSTV.sh:564 aCSTV.sh:813 aCSTV.sh:826
#: aCSTV.sh:918 aCSTV.sh:973 aCSTV.sh:1022 aCSTV.sh:1023 aCSTV.sh:1094
#: aCSTV.sh:1095 aCSTV.sh:1117 aCSTV.sh:1118 aCSTV.sh:1130 aCSTV.sh:1155
#: aCSTV.sh:1170 aCSTV.sh:1190 aCSTV.sh:1196
msgid "aCSTV antiX Community Simple TV Starter"
msgstr ""

# Tray Icon tooltip and upper Window border text of dialog for editing
# stations list
#: aCSTV.sh:192 aCSTV.sh:193 aCSTV.sh:194 aCSTV.sh:784
msgid "aCSTV Senderliste verwalten"
msgstr ""

# Tray Icon tooltip and upper Window border text of dialog for modifying
# settings
#: aCSTV.sh:197 aCSTV.sh:198 aCSTV.sh:199 aCSTV.sh:409 aCSTV.sh:536
msgid "aCSTV Einstellungen verwalten"
msgstr ""

# This is a name for a symbolic link to be generated inside aCSTV config directory in user's home folder. Please make sure to comply in translation with linux file naming conventions.
#: aCSTV.sh:209 aCSTV.sh:210 aCSTV.sh:211
msgid "Senderlisten-Bibliothek"
msgstr ""

# header of an GUI error message dialog.
#: aCSTV.sh:278
msgid "aCSTV — Fehler"
msgstr ""

# Error message in case the manual was not translated to the users UI
# language yet.
#: aCSTV.sh:279
msgid ""
"Die Bedienungsanleitung für <i>aCSTV</i> wurde noch nicht in Ihre Sprache "
"übersetzt."
msgstr ""

# Error message in case the manual was not translated to the users UI
# language yet.
#: aCSTV.sh:279
msgid ""
"Bitte wählen Sie eine andere Sprache aus, in der die Programmhilfe angezeigt "
"werden wird."
msgstr ""

# Name of language selection Pulldown menu
#: aCSTV.sh:281
msgid "Verfügbare Sprachen:"
msgstr ""

# button engraving
#: aCSTV.sh:282
msgid "Auswahl akzeptieren"
msgstr ""

# Command line error message.
#: aCSTV.sh:296
msgid "This should never happen!"
msgstr ""

# Command line error message.
#: aCSTV.sh:296
msgid "Please make sure script was installed properly."
msgstr ""

# Timetable entry.
# This entry is meant to be translated in the following way: Check the
# site coming up from the original URL in your browser and select a
# similar one in your language, providing a time table of FTA-TV stations
# of your country and language freely accessible to public (not behind a paywall)
#: aCSTV.sh:316
msgid "https://www.klack.de/fernsehprogramm/alles-auf-einen-blick/-1/free.html"
msgstr ""

# Base file name for recorded video files. Station name, recording date and
# file type will be added by script along with all separators as needed.
#: aCSTV.sh:460
msgid "Sendungsmitschnitt"
msgstr ""

# Base file name for taken photo files. Station name, recording date and file
# type will be added by script, along with all separators as needed.
#: aCSTV.sh:465 aCSTV.sh:1001 aCSTV.sh:1103 aCSTV.sh:1124
msgid "Szenenfoto"
msgstr ""

# Header of a GUI dialog and window title
#: aCSTV.sh:469 aCSTV.sh:473 aCSTV.sh:493 aCSTV.sh:1213 aCSTV.sh:1297
#: aCSTV.sh:1298 aCSTV.sh:1302
msgid "aCSTV Aufnahme"
msgstr ""

# Text of warning poster coming up when starting a recording being short of
# disk space
#: aCSTV.sh:469
msgid ""
"Achtung! Es ist nur noch Speicherplatz \\n\\tfür 10 Minuten Aufnahmezeit "
"verfügbar."
msgstr ""

# System tray recording tooltip
#: aCSTV.sh:474 aCSTV.sh:479
msgid "Laufende aCSTV Aufzeichnung"
msgstr ""

# Header of a GUI dialog
#: aCSTV.sh:537
msgid "aCSTV Einstellungen"
msgstr ""

# Text block of a GUI dialog. Part 1
#: aCSTV.sh:537
msgid "Bitte geben sie die gewünschten Voreinstellungen ein bzw. wählen"
msgstr ""

# Text block of a GUI dialog. Part 2
#: aCSTV.sh:537
msgid "aus den Menüs die entsprechenden Optionen aus."
msgstr ""

# Name of entry field in a GUI dialog. See pdf help text for its meaning.
#: aCSTV.sh:542
msgid "Max. Videobitrate (in kbps):"
msgstr ""

# Name of entry field in a GUI dialog. Refers to the screen number to be used
# for displaying TV, e.g. 0 or 1
#: aCSTV.sh:543
msgid "Ausgabegerät (Bildschirm):"
msgstr ""

# Name of entry field in a GUI dialog. Referring to the URL pointing to the
# local program information web site.
#: aCSTV.sh:544
msgid "URL für Programminformationen:"
msgstr ""

# Checkbox in a GUI Dialog, refers to initial display mode of TV on screen.
#: aCSTV.sh:545
msgid "Starte Wiedergabe mit Vollbild"
msgstr ""

# Name of entry field in a GUI dialog. Refers to the directory where aCSTV
# will store any screenshots.
#: aCSTV.sh:546
msgid "Verzeichnis für Szenenfotos:"
msgstr ""

# Name of entry field in a GUI dialog. Refers to the file format of any
# screenshots (e.g.png or jpeg)
#: aCSTV.sh:547
msgid "Ausgabeformat für Szenenfotos:"
msgstr ""

# Name of entry field in a GUI dialog. Referring to the directiory to be used
# by aCSTV for storing recordings of the live TV stream.
#: aCSTV.sh:548
msgid "Verzeichnis für Sendungsmitschnitte:"
msgstr ""

# Checkbox in a GUI Dialog, refers to initial display mode of TV on screen.
#: aCSTV.sh:549
msgid "Wiedergabe im Vordergrund"
msgstr ""

# Button engraving
#: aCSTV.sh:550
msgid "Aktualisieren"
msgstr ""

# Button engraving
#: aCSTV.sh:550
msgid "Programmhilfe"
msgstr ""

# Button engraving
#: aCSTV.sh:550
msgid "Senderliste bearbeiten"
msgstr ""

# Button engraving
#: aCSTV.sh:550
msgid "Speichern"
msgstr ""

# GUI error dialog message, window border title
#: aCSTV.sh:610 aCSTV.sh:620
msgid "Fehler"
msgstr ""

# GUI error dialog message, header line in bold letters
#: aCSTV.sh:611 aCSTV.sh:621
msgid "Keine Schreibrechte für die Stationslistendatei."
msgstr ""

# GUI error dialog, message text. Indented by a single tab. Please make sure
# your translation doesn't break dialog layout. You can use as many lines you need.
# Each new line MUST be started with the control code sequence "\\n\\t" (without
# quotes, and replacing a blank each. No additional blanks before or after the
# control seqences please, these would break proper alignement.)
#: aCSTV.sh:611 aCSTV.sh:621
msgid ""
"Bitte kopieren Sie die gewünschte Stationsliste\\n\\taus der für alle Nutzer "
"verfügbaren Bibliothek in\\n\\tIhr Home-Verzeichnis um sie bearbeiten "
"oder\\n\\taktualisieren zu können, und benennen Sie sie\\n\\tIhren Wünschen "
"entsprechend."
msgstr ""

# Informational GUI message
#: aCSTV.sh:660
msgid "Exzessive Senderliste."
msgstr ""

# Informational GUI message
#: aCSTV.sh:660
msgid ""
"Die Verarbeitung von Senderlisten mit mehr als 50-60 aktiven\\nEinträgen "
"kann die Reaktionszeit von aCSTV erheblich verlangsamen.\\n"
msgstr ""

# Informational GUI message
#: aCSTV.sh:660
msgid ""
"Bitte erwägen Sie, Ihre Senderliste zugunsten einer "
"beschleunigten\\nReaktion in verschiedenen aCSTV Funktionen zu beschränken. "
msgstr ""

# Informational GUI message
#: aCSTV.sh:660
msgid ""
"Dies\\nbetrifft u.a. den Programmstart und die Bearbeitung von Senderlisten."
msgstr ""

# Informational GUI message
#: aCSTV.sh:660
msgid "Bitte gedulden Sie sich, bis die Verarbeitung abgeschlossen ist."
msgstr ""

# Header of a GUI dialog
#: aCSTV.sh:785
msgid "aCSTV Senderliste"
msgstr ""

# Text block of a GUI dialog. Part 1
#: aCSTV.sh:785
msgid "Alle Einträge lassen sich per Doppelklick bearbeiten."
msgstr ""

# Text block of a GUI dialog. Part 2
# The words between the italic tags <i>...</i> must point to the translation
# of the respective button engraving above.
#: aCSTV.sh:785
msgid ""
"Sollen Einträge entfernt werden, diese mit einem Häkchen markieren und die "
"Taste <i>»Einträge Entfernen«</i> drücken."
msgstr ""

# Text block of a GUI dialog. Part 3
# The words between the italic tags <i>...</i> must point to the translation
# of the respective button engraving above.
#: aCSTV.sh:785
msgid ""
"Neue Einträge mit der Taste <i>»Neuen Eintrag hinzufügen«</i> anlegen und "
"anschließend bearbeiten."
msgstr ""

# Header entry of an list field referring to the consecutive numbers of the
# list entries.
#: aCSTV.sh:790
msgid "Nr."
msgstr ""

# Header entry of an list field referring to the Names of the TV stations
# listed.
#: aCSTV.sh:790
msgid "Sender"
msgstr ""

# Header entry of an list field referring to the URLs of the respective live
# stream address of the TV stations listed.
#: aCSTV.sh:790
msgid "Adresse (URL)"
msgstr ""

# Button engraving
#: aCSTV.sh:792
msgid "Markierte Einträge löschen"
msgstr ""

# Button engraving
#: aCSTV.sh:793
msgid "Neuen Eintrag hinzufügen"
msgstr ""

# Button engraving
#: aCSTV.sh:794
msgid "Änderungen verwerfen"
msgstr ""

# Button engraving
#: aCSTV.sh:795
msgid "Senderliste speichern"
msgstr ""

# GUI dialog header and window title
#: aCSTV.sh:857 aCSTV.sh:858
msgid "aCSTV Senderliste aktualisieren"
msgstr ""

# GUI dialog window title
#: aCSTV.sh:857
msgid "Sender prüfen"
msgstr ""

# GUI dialog text, followed by a ticker line displaying recently checked
# station.
#: aCSTV.sh:858
msgid "Verbindungsaufbau zum Sender wird geprüft:"
msgstr ""

# Button engraving
#: aCSTV.sh:859 aCSTV.sh:872
msgid "Abbrechen"
msgstr ""

# GUI dialog window title
#: aCSTV.sh:867
msgid "Stationsliste aktualisieren"
msgstr ""

# GUI dialog header
#: aCSTV.sh:868
msgid "aCSTV Stationsliste aktualisieren"
msgstr ""

# GUI dialog text
#: aCSTV.sh:868
msgid "Bitte warten Sie, bis die Aktualisierung abgeschlossen ist."
msgstr ""

# GUI dialog pulldown menu descriptor
#: aCSTV.sh:870
msgid "Länderlisten:"
msgstr ""

# GUI dialog checkbox descriptor
#: aCSTV.sh:871
msgid "Prüfe auf Verfügbarkeit"
msgstr ""

# Button engraving
#: aCSTV.sh:872
msgid "Aktualisierung starten"
msgstr ""

# Part of GUI dialog status message ticker, followed by a stations URL.
#: aCSTV.sh:906
msgid "Prüfe"
msgstr ""

# header of an GUI error message dialog.
#: aCSTV.sh:955
msgid "Verbindungsfehler"
msgstr ""

# text block of an GUI error message dialog.
#: aCSTV.sh:956
msgid "Keine Internetverbindung gefunden."
msgstr ""

# text block of an GUI error message dialog. Part 1
#: aCSTV.sh:956
msgid "Stellen Sie bitte eine Internetverbindung her, bevor"
msgstr ""

# text block of an GUI error message dialog. Part 2
#: aCSTV.sh:956
msgid "Sie es erneut versuchen, oder Sie können aCSTV beenden."
msgstr ""

# Button engraving
#: aCSTV.sh:957 aCSTV.sh:1337
msgid "aCSTV beenden"
msgstr ""

# Button engraving
#: aCSTV.sh:957
msgid "Erneut versuchen"
msgstr ""

# Header of main dialog with all station buttons.
#: aCSTV.sh:975
msgid "TV-Sender"
msgstr ""

# Button tool tip in main dialog
#: aCSTV.sh:978
msgid "vorherige Seite"
msgstr ""

# Button symbol, not meant to be translated. Merely here in case not
# comprehensible in a specific language, in this case it can be replaced
# by translators to a symbol understood in the language concerned.
#: aCSTV.sh:979
msgid " ← "
msgstr ""

# Buttons descriptor between ← → Symbols. Line reading completely:
#   ←  Page  →
#: aCSTV.sh:983
msgid "Seite"
msgstr ""

# Button tool tip in main dialog
#: aCSTV.sh:985
msgid "nächste Seite"
msgstr ""

# Button symbol, not meant to be translated. Merely here in case not
# comprehensible in a specific language, in this case it can be replaced
# by translators to a symbol understood in the language concerned.
#: aCSTV.sh:986
msgid " → "
msgstr ""

# Button tool tip, will be followed by station name
#: aCSTV.sh:996
msgid "Ein- oder Umschalten zu Sender"
msgstr ""

# Button tool tip
#: aCSTV.sh:1034
msgid "Aktuelle Programmvorschau im Browser zeigen"
msgstr ""

# Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV.sh:1035
msgid " Programmvorschau "
msgstr ""

# Button tool tip
#: aCSTV.sh:1038
msgid "aCSTV Grundeinstellungen vornehmen"
msgstr ""

# Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV.sh:1039
msgid " Einstellungen "
msgstr ""

# Button tool tip
#: aCSTV.sh:1043
msgid "Laufendes Programm ausschalten"
msgstr ""

# Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV.sh:1044
msgid " Stop "
msgstr ""

# Button tool tip
#: aCSTV.sh:1063
msgid "aCSTV verlassen"
msgstr ""

# Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV.sh:1064
msgid " Beenden "
msgstr ""

# Button tool tip
#: aCSTV.sh:1072
msgid "Senderliste wechseln"
msgstr ""

# Button tool tip
#: aCSTV.sh:1077
msgid "Videomitschnitt der aktuellen Filmszene anfertigen"
msgstr ""

# Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV.sh:1078
msgid " Videoaufnahme "
msgstr ""

# Button tool tip
#: aCSTV.sh:1097
msgid "Laufenden Videomitschnitt stoppen"
msgstr ""

# Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV.sh:1098
msgid " Aufnahme Stop "
msgstr ""

# Button tool tip
#: aCSTV.sh:1120
msgid "Standbild der aktuellen Filmszene aufnehmen"
msgstr ""

# Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV.sh:1121
msgid " Szenenfoto "
msgstr ""

# Window border title of preview window coming up when pressing the Button
# »Szenenfoto«,
# followed by a long dash and the filename and -path where it was stored to.
#: aCSTV.sh:1124
msgid "Szenenfoto-Vorschau"
msgstr ""

# Button tool tip
#: aCSTV.sh:1181 aCSTV.sh:1182
msgid "aCSTV Senderliste wechseln"
msgstr ""

# Text in GUI Dialog
#: aCSTV.sh:1182
msgid "Bitte wählen Sie die gewünschte Senderliste aus dem Menü."
msgstr ""

# Pulldown menu descriptor in GUI Dialog
#: aCSTV.sh:1186
msgid "Senderliste:"
msgstr ""

# Button engraving
#: aCSTV.sh:1187
msgid "Liste wechseln"
msgstr ""

# Abbreviation of the word "approximately". Needs to be that short.
#: aCSTV.sh:1255 aCSTV.sh:1257 aCSTV.sh:1259 aCSTV.sh:1261 aCSTV.sh:1263
msgid "ca."
msgstr ""

# Part of a number unit format displayed in a sentence in GUI Dialog.
#: aCSTV.sh:1255
msgid "Jahren"
msgstr ""

# Part of a number unit format displayed in a sentence in GUI Dialog.
#: aCSTV.sh:1257
msgid "Monaten"
msgstr ""

# Part of a number unit format displayed in a sentence in GUI Dialog.
#: aCSTV.sh:1259
msgid "Tagen"
msgstr ""

# Part of a number unit format displayed in a sentence in GUI Dialog.
# Abreviation of the word "Hours"
#: aCSTV.sh:1261
msgid "Std."
msgstr ""

# Part of a number unit format displayed in a sentence in GUI Dialog.
# Abreviation of the word "Minutes"
#: aCSTV.sh:1263
msgid "Min."
msgstr ""

# Part of a number unit format displayed in a sentence in GUI Dialog.
# Abreviation of the word "Seconds"
#: aCSTV.sh:1265 aCSTV.sh:1274 aCSTV.sh:1276
msgid "Sek."
msgstr ""

# 1st part of a sentence in GUI dialog reading "Der Mitschnitt der laufenden
# Sendung wird in der Datei x im Verzeichnis y gespeichert."
#: aCSTV.sh:1298
msgid "Der Mitschnitt der laufenden Sendung wird in der Datei"
msgstr ""

# 2nd part of a sentence in GUI dialog
#: aCSTV.sh:1298
msgid "im Verzeichnis"
msgstr ""

# 3rd part of a sentence in GUI dialog
#: aCSTV.sh:1298
msgid "gespeichert. "
msgstr ""

# Text in GUI dialog, followed by a File size value
#: aCSTV.sh:1298
msgid "Gegenwärtige Dateigröße:"
msgstr ""

# 1st part of a sentence in GUI dialog reading "Der verfügbare Speicherplatz
# von a Bytes reicht bei der aktuellen durchschnittlichen Datenrate von b
# kB/Sek. näherungsweise für eine Aufzeichnung von c Std."
#: aCSTV.sh:1298
msgid "Der verfügbare Speicherplatz von"
msgstr ""

# 2nd part of a sentence in GUI dialog
#: aCSTV.sh:1298
msgid "reicht bei der\\n\\taktuellen durchschnittlichen Datenrate von"
msgstr ""

# 3rd part of a sentence in GUI dialog
#: aCSTV.sh:1298
msgid "näherungsweise für eine Aufzeichnung von"
msgstr ""

# Text in GUI dialog
#: aCSTV.sh:1298
msgid ""
"Zum Beenden der Aufnahme drücken Sie bitte im\\n\\taCSTV Hauptfenster die "
"Taste <i>»Aufnahme Stop«</i>."
msgstr ""

# Warning banner window border title
#: aCSTV.sh:1328 aCSTV.sh:1335
msgid "aCSTV Senderliste – Fehler"
msgstr ""

# Warning bannertext
#: aCSTV.sh:1329
msgid ""
"Stationsliste enthält keine aktiven Einträge.\\nBitte aktivieren Sie "
"mindestens einen Eintrag\\noder fügen einen gültigen Eintrag hinzu."
msgstr ""

# Button engraving in warning banner
#: aCSTV.sh:1330
msgid "Einträge bearbeiten"
msgstr ""

# Warning bannertext
#: aCSTV.sh:1336
msgid "Stationsliste ohne gültige Einträge.\\nAbbruch auf Nuztzerwunsch."
msgstr ""
